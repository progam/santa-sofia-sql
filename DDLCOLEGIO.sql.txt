
USE `colegioss` ;

CREATE TABLE Usuario(
	idUsuario INT AUTO_INCREMENT NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	apellido VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY(idUsuario)
	
);

CREATE TABLE TipoDeUsuario(
	idTipoUsuario INT AUTO_INCREMENT NOT NULL,
	idUsuario INT NOT NULL,
	tipo VARCHAR(255) NOT NULL,
    PRIMARY KEY(idTipoUsuario),
	FOREIGN KEY(idUsuario) REFERENCES usuarios(idUsuario)on update cascade
);

CREATE TABLE Maestro(
	idMaestro INT auto_increment NOT NULL,
    idTipoUsuario INT NOT NULL,
    nombre VARCHAR(255)NOT NULL,
    apellido VARCHAR(255)NOT NULL,
    PRIMARY KEY(idMaestro),
    FOREIGN KEY(idTipoUsuario) REFERENCES TipoDeUsuario(idTipoUsuario)ON UPDATE CASCADE
);

CREATE TABLE Administrador(
	idAdministrador INT AUTO_INCREMENT NOT NULL,
    idTipoUsuario INT NOT NULL,
	nombre VARCHAR(255)NOT NULL,
    apellido VARCHAR(255)NOT NULL,
    cargo VARCHAR(255)NOT NULL,
    PRIMARY KEY(idAdministrador),
    FOREIGN KEY(idTipoUsuario) REFERENCES TipoDeUsuario(idTipoUsuario)ON UPDATE CASCADE
);

CREATE TABLE Alumno(
	idAlumno	INT AUTO_INCREMENT NOT NULL,
    idTipoUsuario INT AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(255)NOT NULL,
    apellido VARCHAR(255)NOT NULL,
    edad VARCHAR(255)NOT NULL,
    sexo VARCHAR(255)NOT NULL,
    PRIMARY KEY(idAlumno),
    FOREIGN KEY(idTipoUsuario) REFERENCES TipoDeUsuario(idTipoUsuario)ON UPDATE CASCADE	
);

CREATE TABLE Materia(
	idMateria INT AUTO_INCREMENT NOT NULL,
    idMaestro INT NOT NULL,
    materia VARCHAR(255)NOT NULL,
    grado VARCHAR(255)NOT NULL,
    seccion VARCHAR(255)NOT NULL,
    horario time(255)NOT NULL,
    PRIMARY KEY(idMateria),
    FOREIGN KEY(idMaestro) REFERENCES Maestro(idMaestro)ON UPDATE CASCADE
    
);

CREATE TABLE Nota(
	idNota INT AUTO_INCREMENT NOT NULL,
    idMateria INT NOT NULL,
    idAlumno INT NOT NULL,
    nota1 INT NOT NULL,
    nota2 INT NOT NULL,
    nota3 INT NOT NULL,
    nota4 INT NOT NULL,
    promedio INT NOT NULL,
    PRIMARY KEY(idNota),
    FOREIGN KEY(idMateria) REFERENCES Materia(idMateria)ON UPDATE CASCADE,
    FOREIGN KEY(idAlumno) REFERENCES Alumno(idAlumno)ON UPDATE CASCADE
);
